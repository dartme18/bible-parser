#include "stdafx.hpp"
#include "util.hpp"

void typeset_chapter(uint chapter, std::ostream & str)
{
    if (chapter > 1)
    {
        str << "\\vspace{.2cm}\\\\";
    }
    str << "\\textbf{\\huge " << chapter << "} ";
}

void typeset_verse(uint verse, std::ostream & str)
{
    str << "\\textsuperscript{" << verse << "}";
}

void typeset_lsv(void)
{
    std::ofstream out{"lsv.tex"};
    out << "\\documentclass{book}\n\\usepackage{multicol}\n";
    out << "\\usepackage[left=1cm,right=1cm,top=1.1cm,bottom=1cm]{geometry}\n";
    out << "\\begin{document}\n";
    std::ifstream in_stream("../../lsv_book_token.json");
    rapidjson::IStreamWrapper input_wrapper(in_stream);
    rapidjson::Document d;
    d.ParseStream(input_wrapper);
    // When a line break shows up in a verse, this is set to true. In that case,
    // we will want a line break before the next verse.
    bool line_break{false};
    for (char const * book : std::vector<char const *>{"ge", "ex", "ps"})
    {
        unsigned next_verse{2};
        unsigned chapter{1};
        std::string next_chapter_verse {std::to_string(chapter) + ":" + std::to_string(next_verse)};
        rapidjson::Value const & tokens = d[book]["tokens"];
        rapidjson::Value const & refs = d[book]["refs"];
        unsigned token_num{0};
        unsigned next_verse_token{refs[next_chapter_verse.c_str()].GetUint()};
        out << "\\chapter*{" << get_book_name(book) << "}\\begin{multicols}{2}\\noindent";
        typeset_chapter(chapter, out);
        // MemberBegin iterates object members; Begin iterates array elements
        for (auto i {tokens.Begin()}; i != tokens.End(); ++i)
        {
            rapidjson::Document::ConstMemberIterator member;
            if ((member = i->FindMember("word")) != i->MemberEnd())
                out << member->value.GetString();
            else if ((member = i->FindMember("punctuation")) != i->MemberEnd())
                out << member->value.GetString();
            else if ((member = i->FindMember("layout")) != i->MemberEnd())
            {
                if (member->value.IsString())
                {
                    if (!strcmp("space", member->value.GetString()))
                        out << ' ';
                    else
                        ERROR("token {} unknown layout {}.", token_num, member->value.GetString());
                }
                else if (member->value.IsObject())
                {
                    auto const layout_member {member->value.FindMember("newLine")};
                    if (layout_member != member->value.MemberEnd())
                    {
                        out << "\n\n";
                        line_break = true;
                    }
                    else
                        ERROR("No newline in layout member");
                }
            }
            else
                ERROR("Failed to deal with token {}", token_num);
            ++token_num;
            if (token_num == next_verse_token)
            {
                out << "\n";
                if (next_verse == 1)
                    typeset_chapter(chapter, out);
                ++next_verse;
                next_chapter_verse = std::to_string(chapter) + ":" + std::to_string(next_verse);
                auto next_verse_member {refs.FindMember(next_chapter_verse.c_str())};
                if (next_verse_member == refs.MemberEnd())
                {
                    next_verse = 1;
                    ++chapter;
                    next_chapter_verse = std::to_string(chapter) + ":" + std::to_string(next_verse);
                    next_verse_member = refs.FindMember(next_chapter_verse.c_str());
                    if (next_verse_member == refs.MemberEnd())
                        break;
                }
                else
                {
                    if (line_break)
                    {
                        out << "\n\n";
                        line_break = false;
                    }
                    if (next_verse != 2)
                        typeset_verse(next_verse-1, out);
                }
                next_verse_token = next_verse_member->value.GetUint();
            }
        }
        out << "\\end{multicols}\n";
    }
    out << "\\end{document}\n";
}

int main(int /*argc*/, char const ** /*args*/)
{
    set_thread_name("main-thread");
    typeset_lsv();
    return 0;
}
